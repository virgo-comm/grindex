---
title: Downloads
---

Take a look at some of our past events and get your copies of the brochure and the registration form.

- [Exhibition Brochure 2022](https://content.iptex-grindex.com/documents/2022/IPTEX-GRINDEX-2022.pdf)
- [Application Form 2022](https://content.iptex-grindex.com/documents/2022/space-booking-form.pdf)
- [Retrospect 2020](https://content.iptex-grindex.com/documents/2022/IPTEX-GRINDEX-Retrospect-2020.pdf)
- [Retrospect 2018](https://content.iptex-grindex.com/documents/2022/IPTEX-GRINDEX-Retrospect-2018.pdf)
- [Retrospect 2016](https://content.iptex-grindex.com/documents/2022/IPTEX-GRINDEX-Retrospect-2016.pdf)

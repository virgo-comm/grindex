---
title: Privacy Policy
aliases:
- privacy
---

## About this Privacy Policy

At Virgo Communications & Exhibitions Pvt Ltd, one of our main priorities is the privacy of our visitors. This Privacy Policy document contains types of information that is collected and recorded by IPTEX & GRINDEX 2020 and how we use it.

Information we collect

We collect information about you in three ways:

1. From your input through our web forms, app forms and other mechanisms
2. From third-party sources
3. Through automated technologies

## Data

The types of personal information that we collect directly from you depends on how you interact with us and the Service and may include:

- Contact details, such as your name, email address, postal address, social media handle and phone number;
- Comments, feedback and other information you provide to us, including information that you send to the marketing team for your participation and for interest in business matchmaking (fixing meetings at the exhibition)

We also may obtain personal information about you from third parties, including:

## Media partners

- Paid partnership with marketing agencies/companies
- Publicly-available sources and data suppliers from which we obtain data to validate or supplement the information we hold

## Use of information

- Registration for the exhibition
- Access to business matchmaking program
- Providing information for future vents/exhibitions
- Contact for feedback and testimonial

## Online Privacy Policy Only

This Privacy Policy applies only to our online activities and is valid for visitors to our website with regards to the information that they shared and/or collect in IPTEX & GRINDEX. This policy is not applicable to any information collected offline or via channels other than this website.

## Children’s Information

Another part of our priority is adding protection for children while using the internet. We encourage parents and guardians to observe, participate in, and/or monitor and guide their online activity.

Virgo does not knowingly collect any Personal Identifiable Information from children under the age of 13. If you think that your child provided this kind of information on our website, we strongly encourage you to contact us immediately and we will do our best efforts to promptly remove such information from our records.

## Consent

By using our website, you hereby consent to our Privacy Policy and agree to its Terms and Conditions.

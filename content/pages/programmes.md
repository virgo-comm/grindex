---
title: Programmes
aliases:
- '/programs/'
- '/program'
- '/programme/'
---

## 1-1 Meeting

1-1 Meeting is an exclusive program of GRINDEX 2022 which enables the 
pre-registered visitors of the exhibition to schedule meetings in advance with the gear & power transmission companies exhibiting at the event in Pune.  
1-1 Meeting is the perfect solution to enhance your participation as it enables you to:

1. Research in advance about the exhibiting companies
2. Fix meeting in advance with the desired company
3. Manage your time effectively at the exhibition

[Pre-register now](/register/) to schedule your meetings with the exhibitors of GRINDEX 2022.

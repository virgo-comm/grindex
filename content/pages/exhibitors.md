---
title: Exhibitors
---

The need to adopt new innovations, solutions and applications is ever growing in the manufacturing industry. GRINDEX 2022 presents a window of opportunity for the industry leaders of grinding technology and Surface Finishing Process, to showcase their offerings at a platform open to the manufacturing industry.

Here is a look at the detailed exhibitor profile of the event in Pune:

## Grinding Machines

- Centerless Grinding
- Cylindrical Grinding
- External Grinding
- Grinding Machine for special purpose
- Internal Grinding
- Surface Grinding

## Gear Grinding Machines

- Cutting tools and Precision tools
- Honing, Lapping and deburring machines for cylindrical and gear technology
- Micro Finishing Machines
- Profile Grinding
- Super Finishing Machines

## Super Abrasives & Surface Finishing Solutions

- Bonded Abrasives
- Coated Abrasives
- Diamond Dressers, PCD/PCBN Tools
- Fine Grinding Wheels
- Hones
- Lubricants & Cutting Fluids
- Power Tools

## Industrial Grinding Wheels

- Centreless Grinding Wheels
- Cylindral Grinding Wheels
- Internal Grinding Wheels
- Gear Grinding Wheels
- General Purpose Grinding Wheels

## IOT & Smart Technology

- Artificial Intelligence
- CAD / CAM / CAE
- Data Analytics & Cloud Services
- IoT Devices, Sensors and Components
- Smart Factory Solutions

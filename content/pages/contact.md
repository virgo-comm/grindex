---
title: "Contact"
---

## Organiser

250 Virgo House,  
7th Main, Amarjyoti Layout,  
Domlur Extension,  
Bangalore 560071, India

T: +91 80 25357028 / 29, 41493996 / 97  
M: 9740611121

www.virgo-comm.com

For more information please write to us at  
&#100;&#101;&#118;&#64;&#118;&#105;&#114;&#103;&#111;&#45;&#99;&#111;&#109;&#109;&#46;&#99;&#111;&#109;

---
title: "India's only dedicated show for the Gear, Power Transmission, Grinding Technology & Finishing Process industry"
date: "2021-11-07"
author: "Team Virgo"
image: https://content.iptex-grindex.com/images/news/iptex-grindex-20211108.jpg
---

Virgo Communications & Exhibitions (P) Ltd. has announced the next edition of their exclusive b2b exhibitions for the industry of gears and grinding technology. The 7th edition of IPTEX: International Power Transmission Expo, and GRINDEX: International Expo on Grinding & Finishing Process, is scheduled from the 21st to 23rd April 2022 at the Auto Cluster Exhibition Centre, Pimpri-Chinchwad, Pune, Maharashtra.

Talking about IPTEX 2022, Ms. Anitha Raghunath, Managing Director of Virgo Communications & Exhibitions (P) Ltd. said, “IPTEX has established itself as the most dedicated platform for the Gears and Power Transmission Equipment industry, enabling the industry players to network with their peers & potential buyers and to market their products, services, and capabilities to a diverse and significant audience. After five editions of IPTEX in Mumbai and the first edition of the expo in Pune last year, we are very excited about bringing back the seventh edition of the expo with an exhibit of innovations by over 125 leading manufacturers and suppliers of technologies for the entire power transmission industry. And what better place to showcase this business opportunity but in the city of manufacturing & automobile hub of India: Pune.”

Also speaking about their association with the American Gears Manufacturers Association, Anitha said, “It is always a pleasure to be associated with the team of AGMA, and this is going to be the fourth edition of our association with them and similar to the last edition we hope to create an environment of learning & knowledge sharing forum through the seminar during the expo.” 

Speaking about their participation during the 6th edition of IPTEX, Mr. Matthew Croson, President of American Gear Manufacturers’ Association (AGMA) had said, “It’s a big business opportunity to be in India. This is my 3rd time in India, and people here inspire me a lot by their thoughts. Working in India is always a good experience and as we all see the Gear Grinding Market is expected to increase rapidly in India in 2020”. He added further that “Innovation is born in a state of alignment with the production of the needs and demands of the customer, or the other way around. In any case, organizations around the globe must continually develop themselves and remain conscious of the needs of the people. The failure to do so or not addressing the needs of your company would make your rivals win”. He also said “making new things is something that always creates innovation in your mind, and that’s what we’re doing here- we are innovating people’s minds with the help of machine and technology.”

Talking about the concurrently held expo for the grinding technology, Mr. Raghu G, Director of Virgo Communications & Exhibitions (P) Ltd. said, “With four successful editions of GRINDEX, the expo is recognised by the global industry stalwarts as one of the most ideal business platforms for showcasing latest industrial products & technology in India, dedicated to the Tool Grinding & Surface finishing technology.”

He further added that “GRINDEX is specifically designed to offer an opportunity for the manufacturing industry to explore new high-end technology, solutions, and applications, with the prime focus on increasing the cost effectiveness and process efficiency.”

IPTEX-GRINDEX 2022 will present a showcase of innovation by over 125 companies from the gear & grinding industry, with participation from 9 countries and the past events are proof that the exhibition presents business solutions for over 47 industries including automotive, machine tools, metalworking, construction, pharmaceutical, railway, food processing, steel, textile among many more.
